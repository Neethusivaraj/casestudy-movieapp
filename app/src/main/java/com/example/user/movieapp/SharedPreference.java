package com.example.user.movieapp;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by user on 1/4/2016.
 */
public class SharedPreference {
    public static final String PREFS_NAME = "MOVIE_APP";
    public static final String FAVORITES = "Movie_Favorite";
    public static final String MY_LIST = "my_list";
    private List<String> myList = new ArrayList<String>();

    public SharedPreference() {
        super();
    }

    public void saveFavorites(Context context, List<String> myList) {
        SharedPreferences settings;
      //  SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    /*    editor = settings.edit();

        editor.putString(FAVORITES, favorites);

        editor.commit();
*/      String jsonFavorites = new Gson().toJson(myList);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(MY_LIST, jsonFavorites);
        myList.add(jsonFavorites);
        editor.commit();
        System.out.println("save Fav               :" + myList);

    }

    public List<String> getValue(Context context) {
        SharedPreferences settings;
        //String text;

        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String jsonFav = settings.getString(MY_LIST, null);
        //text = settings.getString(FAVORITES, null);
        Type t = new TypeToken<List<String>>() {}.getType();
        myList = new Gson().fromJson(jsonFav, t);
        myList.add(jsonFav);
        System.out.println("My List     :" + myList);
        saveFavorites(context, myList);
        return myList;

    }

    public void clearSharedPreference(Context context) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.clear();
        editor.commit();
    }

    public void removeValue(Context context) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.remove(FAVORITES);
        editor.commit();
    }
}


/*
    public static final String PREFS_NAME = "MOVIE_APP";
    public static final String FAVORITES = "Favorite";
    public SharedPreference() {
        super();
    }
    public void storeFavorites(Context context, List favorites) {

        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(PREFS_NAME,Context.MODE_PRIVATE);
        editor = settings.edit();
        //Gson gson = new Gson();
        Exclude ex = new Exclude();
        Gson gson = new GsonBuilder().create();
        //.addDeserializationExclusionStrategy(ex).addSerializationExclusionStrategy(ex)
        String jsonFavorites = gson.toJson(favorites);
        editor.putString(FAVORITES, jsonFavorites);
        favorites.add(jsonFavorites);
        editor.commit();
    }
    public ArrayList loadFavorites(Context context) {
        // used for retrieving arraylist from json formatted string
        SharedPreferences settings;
        List<String> favorites = new ArrayList<String>();

        settings = context.getSharedPreferences(PREFS_NAME,Context.MODE_PRIVATE);
        System.out.println("context :       "+context);
        if (settings.contains(FAVORITES)) {
            //Gson gson = new Gson();
            System.out.println("Contains favourites");
            String jsonFavorites = settings.getString(FAVORITES, null);
            System.out.println(" Settings value " + settings);
            System.out.println(" Settings value " + jsonFavorites);

            //FavouritesActivity favoriteItems = gson.fromJson(jsonFavorites, FavouritesActivity.class);
            Exclude ex = new Exclude();
            Gson gson = new GsonBuilder().addDeserializationExclusionStrategy(ex).addSerializationExclusionStrategy(ex).create();
            Type type = new TypeToken<ArrayList<String>>(){}.getType();
            favorites = gson.fromJson(jsonFavorites, type);

            //favorites = Arrays.asList(favoriteItems);
            //favorites = new ArrayList(favorites);
            System.out.println("favorites :     "+favorites);
        } else
            return null;
        return (ArrayList) favorites;
    }
    public void addFavorite(Context context, List<String> favList) {
        List favorites = new ArrayList<String>();
                favorites = loadFavorites(context);
        if (favorites == null)
            favorites = new ArrayList();
        System.out.println("add Favorites");
        favorites.add(favList);
        storeFavorites(context, favorites);
    }
    public void removeFavorite(Context context, FavouritesActivity favList) {
        ArrayList favorites = loadFavorites(context);
        if (favorites != null) {
            favorites.remove(favList);
            storeFavorites(context, favorites);
        }
    }
}

*/