package com.example.user.movieapp;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 1/4/2016.
 */
public class FavouritesActivity extends Activity {

    private static final String TAG_TITLE = "title";
    private static final String TAG_DESCRIPTION = "description";
    private ListView listView;
    ArrayAdapter<String> adapter;
    private TextView textTxt;
    SharedPreference sharedPreference;
    ArrayList<String> arr = new ArrayList<String>();
    List<String> favorites;
    Context context = this;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourites);
        sharedPreference = new SharedPreference();
        findViewsById();

        //favorites = sharedPreference.loadFavorites(FavouritesActivity.this);
        favorites = sharedPreference.getValue(FavouritesActivity.this);
        System.out.println("aaaaaaaaaaaaaaaaaaaaaaaarrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr" +favorites);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arr);
        arr.add(String.valueOf(favorites));
        listView.setAdapter(adapter);
        //textTxt.setText(text);
        adapter.notifyDataSetChanged();

    }

    /*void getarr(List<String> stringLst)
    {
        System.out.println("stinggggggggggggggggggg listttttttttttttttttttttttt keerrrrrrrrrrrrrrrrrrrrrrrrrrrr"+stringLst);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, stringLst);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }*/

    private void findViewsById() {
        listView = (ListView) findViewById(R.id.list);
        textTxt = (TextView) findViewById(R.id.textView1);
        textTxt.setMovementMethod(new ScrollingMovementMethod());
    }


}

/*    private static final String TAG_TITLE = "title";
    private static final String TAG_DESCRIPTION = "description";
    ArrayList<FavoriteSampleList> postFavSampleList = new ArrayList<FavoriteSampleList>();
    private ListView listView;
    ArrayAdapter<String> adapter;
    private PostsListAdapter postListAdapter;
    private TextView textTxt;
    private String text;
    private SharedPreference sharedPreference;
    Activity context = this;
    int position = 1 ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_favourites);
        sharedPreference = new SharedPreference();
        listView = getListView();
        findViewsById();

        //text = sharedPreference.getValue(context);
        Bundle b = getIntent().getExtras();

        if(b!=null) {
            ArrayList<String> arr = (ArrayList<String>) b.getStringArrayList("array_list");
            System.out.println(arr);
        }

        //ArrayList<String> myList = (ArrayList<String>) getIntent().getSerializableExtra("mylist");
        System.out.println("texttttttttttttttttttttttttttttttt" +text);
        postListAdapter = new PostsListAdapter(this, postFavSampleList);
        //adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);
        //setListAdapter(adapter);
        //listView.getChildAt(position + 1);
        //myList.add(text);
        listView.setAdapter(postListAdapter);
        //textTxt.setText(text);
        adapter.notifyDataSetChanged();

    }

    private void findViewsById() {
        textTxt = (TextView) findViewById(R.id.textView1);
        textTxt.setMovementMethod(new ScrollingMovementMethod());
    }

    public class PostsListAdapter extends BaseAdapter {

        private Context context;
        ArrayList<FavoriteSampleList> postFavSampleList;
        SharedPreference sharedPreference;

        public PostsListAdapter(Context context, ArrayList<FavoriteSampleList> postFavSampleList) {

            this.context = context;
            this.postFavSampleList = postFavSampleList;
            sharedPreference = new SharedPreference();
        }

        private class ViewHolder {
            TextView txtTitle,txtSubTitle;
            ImageView btnFavourite;
        }

        @Override
        public int getCount() {

            return postFavSampleList.size();
        }

        @Override
        public Object getItem(int position) {

            return postFavSampleList.get(position);
        }

        @Override
        public long getItemId(int position) {

            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_posts_list, parent, false);
                holder = new ViewHolder();
                holder.txtTitle = (TextView) convertView
                        .findViewById(R.id.txtPostTitle);
                holder.txtSubTitle = (TextView) convertView
                        .findViewById(R.id.txtPostSubTitle);

                holder.btnFavourite = (ImageView) convertView
                        .findViewById(R.id.favouritesToggle);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            FavoriteSampleList favSampleList = (FavoriteSampleList) getItem(position);
            holder.txtTitle.setText(favSampleList.getTitle());
            holder.txtSubTitle.setText(favSampleList.getSubTitle());


            if (checkFavoriteItem(favSampleList)) {
                Toast.makeText(context, "Active", Toast.LENGTH_SHORT).show();
                holder.btnFavourite.setTag("active");
            } else {
                Toast.makeText(context, "DeActive", Toast.LENGTH_SHORT).show();
                holder.btnFavourite.setTag("deactive");
            }
            holder.btnFavourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    String tag = holder.btnFavourite.getTag().toString();
                    if (tag.equalsIgnoreCase("deactive")) {
                        sharedPreference.addFavorite(context, postFavSampleList.get(position));
                        holder.btnFavourite.setTag("active");
                        Toast.makeText(context, "Active TAGGGGG", Toast.LENGTH_SHORT).show();
                    } else {
                        sharedPreference.removeFavorite(context, postFavSampleList.get(position));
                        holder.btnFavourite.setTag("deactive");
                    }
                }
            });
            return convertView;
        }


        public boolean checkFavoriteItem(FavoriteSampleList checkList) {
            boolean check = false;
            List<FavoriteSampleList> favorites = sharedPreference.loadFavorites(context);
            if (favorites != null) {
                for (FavoriteSampleList product : favorites) {
                    if (product.equals(checkList)) {
                        check = true;
                        break;
                    }
                }
            }
            return check;
        }
    }

}

*/